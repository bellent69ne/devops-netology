# devops-netology

This repository keeps all devops related tasks.

Files that will be ignored:

1. All .terraform directories that we may store anywhere in this repository.
2. All files that have suffix .tfstate or contain .tfstate. in their name.
3. Files with name crash.log.
4. All files that have .tfvars suffix.
5. All files with names override.tf, override.tf.json. Also files that contain _override.tf, _override.tf.json in their names.
6. All .terraformrc and terraform.rc files.

Changing README.md 
And another change.